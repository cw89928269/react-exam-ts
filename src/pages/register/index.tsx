import React, { useEffect, useState } from 'react'
import { Button, Form, Input, message } from 'antd'
import style from './index.module.scss'
import login_desc from './assets/login_desc.png'
import { useNavigate } from 'react-router'
import { useAppDispatch } from '@/store/index';
import { set_user_info } from '@/store/slice/user';
import { registerPost, RegisterBody } from '@/util/request';

const LoginPage: React.FC = () => {
	const [count, set_count] = useState(0)
	const [show, set_show] = useState(true)
	const [form] = Form.useForm()
	const navigate = useNavigate()
	const dispatch = useAppDispatch()

	const onFinishFailed = (errorInfo: any) => {

	}

	async function onRegister(value: RegisterBody) {
		// const res: AxiosResData<any> = await axios.post('/api/user/login', value)

		// @ts-ignore
		const res: any = await registerPost(value)
		if(res.code === 0) {
			message.success('注册成功')	
			navigate('/login')
		} else {
			message.error(res.msg)
		}
	}

	return (
		<div className={style.page_container}>

			<div className={style.login_container}>
				<div className={style.login_left}>
					<div className={style.left_title}>
						<img src={login_desc} alt="" />
					</div>
				</div>
				<div className={style.login_right}>
					<div className={style.right_form}>
						<Form onFinish={onRegister} size="large" labelCol={{ span: 5 }} wrapperCol={{ span: 20 }}  labelAlign="left" onFinishFailed={onFinishFailed} form={form}>
							<Form.Item
								label="用户名"
								name="username"
								rules={[
									{ required: true, message: '请填写用户名' },
								]}
							>
								<Input placeholder="请输入用户名" />
							</Form.Item>
							<div style={{ position: 'relative' }}>
								<Form.Item label="密码" name="password" rules={[{ required: true, message: '请输入密码' }]}>
									<Input type='password' placeholder="请输入密码" style={{ padding: '7px 100px 7px 11px' }} />
								</Form.Item>
							</div>
							<div className={style.form_btn}>
								<Button type="primary" htmlType="submit">
									注册
								</Button>
							</div>
						</Form>
					</div>
				</div>
			</div>

		</div>
	)
}

export default LoginPage
