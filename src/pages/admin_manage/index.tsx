import { useEffect } from 'react';
import { Table, Input, Button, message } from 'antd';
import { useAppDispatch } from '@/store'
import styles from './index.module.css'
import { useSelector } from 'react-redux';
import { get_admin_async, select_user_admin_list } from '@/store/slice/user';
import { useState } from 'react';
import { addAdminRequest, userDelete } from '@/util/request';

function AdminManage() {
    const dispatch = useAppDispatch();
    const admin_list = useSelector(select_user_admin_list)

    const [username, set_username] = useState('')
    const [password, set_password] = useState('')

    useEffect(() => {
        dispatch(get_admin_async())
    }, [])

    async function delete_click(record: any) {
        await userDelete(record._id)
        dispatch(get_admin_async())
    }

    const columns = [
        {
            title: '序号',
            dataIndex: '_id',
            key: '_id',
            sorter: {
                compare: (a: any, b: any) => a!.sort! - b!.sort!,
                multiple: 3,
            },
        },
        {
            title: '用户名',
            dataIndex: 'username',
        },
        {
            title: '操作',
            dataIndex: 'option',
            render: (_: any, record: any) => [
                <a key="delete"
                    onClick={() => {
                        delete_click(record)
                    }}
                >
                    删除
                </a>,
            ],
        }
    ];

    function username_change(e: any) {
        set_username(e.target.value)
    }

    function password_change(e: any) {
        set_password(e.target.value)
    }

    async function add_admin() {
        if (!username) {
            return
        }
        await addAdminRequest({
            username,
            password
        })

        dispatch(get_admin_async())
        set_username('')
        set_password('')
        message.success('新增成功')
    }
    return (
        <div>
            <div className={styles.search_wrap}>
                <Input
                    value={username}
                    onChange={username_change}
                    width="200px"
                    className={styles.input}
                    placeholder='请输入用户名'
                    style={{ marginRight: '20px' }}
                />
                <Input
                    value={password}
                    onChange={password_change}
                    width="200px"
                    className={styles.input}
                    placeholder='请输入密码'
                    type='password'
                />
                <Button className={styles.btn} type='primary' onClick={add_admin}>新增</Button>
            </div>
            <Table
                dataSource={admin_list}
                columns={columns} 
                rowKey='_id'
            />
        </div>
    )
}

export default AdminManage