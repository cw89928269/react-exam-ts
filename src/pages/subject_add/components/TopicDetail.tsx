import CustomUpload from '@/common_components/Upload'
import { useAppDispatch, useAppSelector } from '@/store'
import { select_active_topic, select_active_two, set_subject_active_topic, set_subject_active_two, get_topic_two_list, select_exam_topic_selected } from '@/store/slice/subject'
import { Button, Form, Input, Radio, Checkbox, message, Space } from 'antd'
import { UploadChangeParam, UploadFile, UploadProps } from 'antd/es/upload'
import { useEffect, useState } from 'react'
import request from '@/util/http'
import { upload_imgs } from '@/util'

export default function TopicDetail() {

	// 当前题目类型
	const selected = useAppSelector(select_exam_topic_selected)

	const [loading, setLoading] = useState(false)
	const [fileList, setFileList] = useState<UploadFile[]>([])


	const currentlesson = useAppSelector(select_active_two)

	const currentTopic = useAppSelector(select_active_topic)

	const dispatch = useAppDispatch()
	const [form] = Form.useForm()

	// 组件卸载时把当前选择的数据删除
	useEffect(() => {
		return () => {
			dispatch(set_subject_active_two(null))
			dispatch(set_subject_active_topic(null))
		}
	}, [])

	useEffect(() => {
		if (!currentTopic) {
			reset()
		} else {
			form.setFieldsValue(currentTopic)
			if (currentTopic.img?.length) {
				setFileList(
					currentTopic.img.map((url) => {
						const fileName = url.split('/').at(-1)!
						return {
							uid: fileName,
							name: fileName,
							status: 'done',
							url: '//' + url,
						}
					})
				)
			} else {
				setFileList([])
			}
		}
	}, [currentTopic?._id])

	useEffect(() => {
		if(currentTopic && selected !== currentTopic.topic_type) {
			reset()
		} else {
			form.setFieldsValue(currentTopic)
		}
	}, [selected])

	// 重置表单
	const reset = () => {
		form.resetFields()
		setFileList([])
	}

	const handleImgChange: UploadProps['onChange'] = async (fileInfo: UploadChangeParam) => {
		setFileList(fileInfo.fileList.map((item) => ({ ...item, status: 'done' })))
	}

	const submit = async (data: any) => {
		setLoading(true)

		if (fileList.length) {
			// 需要上传的图片文件（如果没有则不用处理）
			const needUploadImgs = fileList.filter((file) => !file.url)
			if (needUploadImgs.length) {
				const imgURLs = (await upload_imgs(fileList)) as string[]
				data.img = imgURLs
			}
		} else {
			data.img = []
		}

		try {
			// 编辑时 todo：
			if (currentTopic) {
				// 提交数据
				await request.patch(`/api/topic/${currentTopic._id}`, {
					title: data.title,
					img: data.img,
					options: data.options || (selected === 'judge' ?  [{ A: '对', B: '错' }] : []),
					correct_single_option: data.correct_single_option || '',
					analysis: data.analysis || '',
					correct_multiple_option: data.correct_multiple_option || [],
					score: data.score,
					topic_type: selected,
				})
			} else {
				// 新增时
				// 提交数据
				await request.post(`/api/topic`, { 					
					title: data.title,
					img: data.img,
					options: data.options || (selected === 'judge' ?  [{ A: '对', B: '错' }] : []),
					correct_single_option: data.correct_single_option || '',
					analysis: data.analysis || '',
					correct_multiple_option: data.correct_multiple_option || [],
					score: data.score,
					topic_type: selected,
					two_id: currentlesson!.value,
					answer: selected === 'multiple' ?  [] : ''
				})
				reset()
			}
			dispatch(get_topic_two_list(currentlesson!.value))
			message.success('操作成功')
		} catch {
			message.error('操作失败')
		} finally {
			setLoading(false)
		}
	}

	const checkOptions = [
		{ label: 'A', value: 'A' },
		{ label: 'B', value: 'B' },
		{ label: 'C', value: 'C' },
		{ label: 'D', value: 'D' }
	]

	let formListInitValues = [{
		A: '',
		B: '',
		C: '',
		D: ''
	}]

	return (
		<Form form={form} autoComplete="off" name="subject-detail-form" scrollToFirstError onFinish={submit}>
			<Form.Item label="题干" name="title" rules={[{ required: true, message: '题干必填' }]}>
				<Input.TextArea rows={(selected === 'short' ||currentTopic?.topic_type === 'short') ? 6 : 1} />
			</Form.Item>
			{
				selected === 'select' && (
					<>
						<Form.Item label="图片" name="img">
							<CustomUpload
								fileList={fileList}
								uploadProps={{
									onChange: handleImgChange,
								}}
							/>
						</Form.Item>	
						<Form.List name="options" initialValue={formListInitValues}>
							{(fields) => 
								fields.map(({key, name, ...restField}) => (
									<Space key={key}>
										<Form.Item label="A" name={[name, 'A']} rules={[{ required: true, message: '选项内容必填' }]}>
										<Input />
										</Form.Item>
										<Form.Item label="B" name={[name, 'B']} rules={[{ required: true, message: '选项内容必填' }]}>
											<Input />
										</Form.Item>
										<Form.Item label="C" name={[name, 'C']} rules={[{ required: true, message: '选项内容必填' }]}>
											<Input />
										</Form.Item>
										<Form.Item label="D" name={[name, 'D']} rules={[{ required: true, message: '选项内容必填' }]}>
											<Input />
										</Form.Item>
									</Space>
								))
							} 
						</Form.List>
						<Form.Item label="正确选项" name="correct_single_option" rules={[{ required: true, message: '正确选项必填' }]}>
							<Radio.Group>
								<Radio value="A">A</Radio>
								<Radio value="B">B</Radio>
								<Radio value="C">C</Radio>
								<Radio value="D">D</Radio>
							</Radio.Group>
						</Form.Item>
					</>
				)
			}
			{
				selected === 'multiple' && (
					<>
						<Form.Item label="图片" name="img">
							<CustomUpload
								fileList={fileList}
								uploadProps={{
									onChange: handleImgChange,
								}}
							/>
						</Form.Item>	
						<Form.List name="options" initialValue={formListInitValues}>
							{(fields) => 
								fields.map(({key, name, ...restField}) => (
									<Space key={key}>
										<Form.Item label="A" name={[name, 'A']} rules={[{ required: true, message: '选项内容必填' }]}>
										<Input />
										</Form.Item>
										<Form.Item label="B" name={[name, 'B']} rules={[{ required: true, message: '选项内容必填' }]}>
											<Input />
										</Form.Item>
										<Form.Item label="C" name={[name, 'C']} rules={[{ required: true, message: '选项内容必填' }]}>
											<Input />
										</Form.Item>
										<Form.Item label="D" name={[name, 'D']} rules={[{ required: true, message: '选项内容必填' }]}>
											<Input />
										</Form.Item>
									</Space>
								))
							} 
						</Form.List>
						<Form.Item label="正确选项" name="correct_multiple_option" rules={[{ required: true, message: '正确选项必填' }]}>
							<Checkbox.Group
								options={checkOptions}
							/>
						</Form.Item>
					</>
				)
			}
			{
				selected === 'judge' && (
					<>
						<Form.Item label="图片" name="img">
							<CustomUpload
								fileList={fileList}
								uploadProps={{
									onChange: handleImgChange,
								}}
							/>
						</Form.Item>	
						<Form.Item label="正确选项" name="correct_single_option" rules={[{ required: true, message: '正确选项必填' }]}>
							<Radio.Group>
								<Radio value="A">A(对)</Radio>
								<Radio value="B">B(错)</Radio>
							</Radio.Group>
						</Form.Item>
					</>
				)
			}
			{
				selected === 'short' && (
					<>
						<Form.Item label="图片" name="img">
							<CustomUpload
								fileList={fileList}
								uploadProps={{
									onChange: handleImgChange,
								}}
							/>
						</Form.Item>
					</>
				)
			}

			<Form.Item label="分值" name="score" rules={[{ required: true, message: '分值必填' }]}>
				<Input style={{width: '50px'}}></Input>
			</Form.Item>		
			<Form.Item label="解析" name="analysis">
				<Input.TextArea rows={6}></Input.TextArea>
			</Form.Item>

			<Form.Item wrapperCol={{ offset: 2 }}>
				{/* disabled 1.提交时防止重复提交 2.没有课程id保存数据出错 */}
				<Button disabled={loading || !currentlesson?.value} type="primary" htmlType="submit">
					提交
				</Button>
			</Form.Item>
		</Form>
	)
}
