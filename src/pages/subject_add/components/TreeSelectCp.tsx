import { useAppDispatch, useAppSelector } from '@/store';
import {
	get_topic_two_list,
	select_active_two,
	select_subject_tree,
	set_exam_topic_selected,
	set_subject_active_topic,
	set_subject_active_two,
	select_exam_topic_selected
} from '@/store/slice/subject';
import { Button, Radio, RadioChangeEvent, TreeSelect } from 'antd';
import { ReactNode, useEffect, useState } from 'react';
import { SubjectData } from '../../../util/request';

// 禁用含有children字段的项
const disableHasChildrenItem = (items: SubjectData[]) => {
	const _items = JSON.parse(JSON.stringify(items))
	return _items.map((item: SubjectData) => {
		if (item.children?.length > 0) {
			// @ts-ignore
			item.disabled = true
			item.children = disableHasChildrenItem(item.children)
		}
		return item
	})
}

function TreeSelectCp() {

	const onChange = (e: RadioChangeEvent) => {
		dispatch(set_exam_topic_selected(e.target.value))
  };

	const dispatch = useAppDispatch()

	// 当前选择课程
	const selected = useAppSelector(select_exam_topic_selected)

	// 学科列表
	const lessonList = useAppSelector(select_subject_tree)

	// 学科列表memo 使父级不能选择
	const lessonListMemo = lessonList.length ? disableHasChildrenItem(lessonList) : []

	// 卸载删除
	useEffect(() => {
		dispatch(set_subject_active_two(null))
		dispatch(set_subject_active_topic(null))
	}, [])

  // 当前学科
  const currentlesson = useAppSelector(select_active_two)

  // 获取题目列表
	useEffect(() => {
		if (!currentlesson?.value) return
		dispatch(get_topic_two_list(currentlesson.value))
	}, [currentlesson?.value])

	// 选择学科
	const handleLessonChange = (value: string, labelList: ReactNode[]) => {
		dispatch(set_subject_active_topic(null))
		dispatch(
			set_subject_active_two({
				title: labelList[0],
				value: value,
			})
		)
	}

	// 新增题目
	const addTopic = () => {
		dispatch(set_subject_active_topic(null))
		dispatch(set_exam_topic_selected('select'))
	}

    return (
        <div className="title-bar">
            <p className="title">{currentlesson?.title}</p>
            <div className="lesson-select">
                <TreeSelect
                    popupClassName={'subject-add-tree-select'}
                    style={{ width: 320, marginRight: 20 }}
                    treeDefaultExpandAll
                    treeData={lessonListMemo}
                    value={currentlesson?.value}
                    onChange={handleLessonChange}
                />
                <Button type="primary" onClick={addTopic} style={{marginRight: 20}}>
                    点击新增
                </Button>
								<Radio.Group onChange={onChange} value={selected}>
									<Radio value="select">单选题</Radio>
									<Radio value="multiple">多选题</Radio>
									<Radio value="judge">判断题</Radio>
									<Radio value="short">简答题</Radio>
								</Radio.Group>
            </div>
        </div>
    )
}

export default TreeSelectCp