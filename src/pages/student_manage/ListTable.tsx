
import React, { useState, useEffect } from 'react';
import { Table, Divider, Form, message } from 'antd';
import { useAppDispatch } from '@/store'
import { get_student_async, set_checked_sudent_list, set_is_show_user_edit_modal, select_user_student_list_data, select_student_list_search_params, select_student_list_current_page, set_student_list_current_page } from '../../store/slice/user';
import { get_subject_tree_async } from '@/store/slice/subject';
import { userDelete } from '@/util/request';
import dayjs from 'dayjs'
import { useAppSelector } from '../../store/index';
import { Pagination } from 'antd';
import styles from './index.module.css'

const PAGE_COUNT = 10

const ListTable: React.FC = () => {
    const dispatch = useAppDispatch();

    const data = useAppSelector(select_user_student_list_data)
    const search_params = useAppSelector(select_student_list_search_params)
    const student_list = data.list
    
    const count = data.count
    const current_page = useAppSelector(select_student_list_current_page)

    useEffect(() => {
        dispatch(get_subject_tree_async())
        dispatch(get_student_async({}))
    }, [])

    async function delete_click(record: any) {
        await userDelete(record._id)
        dispatch(get_student_async({
            ...search_params,
            skip: (current_page - 1) * PAGE_COUNT,
            limit: PAGE_COUNT
        }))
        message.success('删除成功')
    }

    function page_change(val: any) {
        dispatch(set_student_list_current_page(val))
        dispatch(get_student_async({
            ...search_params,
            skip: (val - 1) * PAGE_COUNT,
            limit: PAGE_COUNT
        }))
    }

    const columns = [
        {
            title: '用户名',
            dataIndex: 'username',
            key: 'username',
            render: (dom: any, entity: any) => {
                return (
                    <a
                        onClick={() => {
                        }}
                    >
                        {dom}
                    </a>
                );
            },
            
        },
        {
            title: '当前薪资',
            dataIndex: 'money',
            key: 'money',
        },
        {
            title: '技术栈',
            dataIndex: 'techStack',
            key: 'techStack',
        },
        {
            title: '学历',
            dataIndex: 'edu',
            key: 'edu',
        },
        {
            title: '微信号',
            dataIndex: 'vChat',
            key: 'vChat',
        },
        {
            title: '课程权限',
            dataIndex: 'role',
            key: 'role',
        },
        {
            title: '注册时间',
            dataIndex: 'created',
            key: 'created',
            render: (_: any, record: any) => {
                return <span>{dayjs(record.created).format('YYYY MM-DD')}</span>
            }
        },
        {
            title: '操作',
            width: 380,
            dataIndex: 'option',
            render: (_: any, record: any) => [
                <a key="delete"
                    onClick={() => {
                        delete_click(record)
                    }}
                >
                    删除
                </a>,
            ],
        },
    ];

    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
            dispatch(set_checked_sudent_list(selectedRows))
        }
    };

    return (
        <>
            <Table
                dataSource={student_list}
                columns={columns}
                pagination={false}
                rowSelection={{
                    type: "checkbox",
                    ...rowSelection
                }}
                rowKey="_id"
            />
            <Pagination
                className={styles.pagenation}
                pageSize={PAGE_COUNT}
                current={current_page}
                total={count}
                onChange={page_change}
            />
        </>
    );
};

export default ListTable;
