import ListTable from "./ListTable";
import Modal from './Modal'
import Search from "./Search";
import useRenderCheck from '../../hooks/renderCheck';

function StudentManage() {
    // 检查组件是否重复渲染
    useRenderCheck('StudentManage')
    return (
        <div className="wrap">
            <Search></Search>
            <ListTable />
            <Modal />
        </div>
    )
}

export default StudentManage