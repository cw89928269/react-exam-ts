import { useAppDispatch } from '@/store';
import { useAppSelector } from '@/store/index';
import { select_subject_tree } from '@/store/slice/subject';
import { SubjectData, examPost, getSingleTopic, getSingleUserInfoRequest, getTopic2List, searchSubject, timeCreate, userInfoPatch } from '@/util/request';
import { Button, Checkbox, DatePicker, Form, Modal, Radio, message } from 'antd';
import { useEffect, useState } from 'react';
import { select_checked_sudent_list, select_is_show_user_edit_modal, set_checked_sudent_list, set_is_show_user_edit_modal } from '../../store/slice/user';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';

function EditModal() {
    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    const [form] = Form.useForm();

    const is_show = useAppSelector(select_is_show_user_edit_modal)
    const subject_tree: SubjectData[] = useAppSelector(select_subject_tree)

    const checked_sudent_list = useAppSelector(select_checked_sudent_list)

    const [checkbox_options, setCheckboxOptions] = useState<any>([])

    const [topic_list, setTopicList] = useState<any>([])

    const [check_list, setCheckList] = useState<any>([])

    const [total_score, setTotalScore] = useState<number>(0)

    useEffect(() => {
      setTotalScore(check_list.reduce((pre: any, cur: any) => pre + parseInt(cur.score), 0))
    }, [check_list])
    
    // 对课程列表树状结构处理，目的： antd的表单能够认识
    let radio_options: any = []
    subject_tree?.forEach((item) => {
        item.children?.forEach((child_item) => {
            radio_options.push({
                label: child_item.title,
                value: child_item.value
            })
        })
    })

    const onCancel = () => {
      dispatch(set_is_show_user_edit_modal(false))
    };

    const onFinish = async(values: any) => {
      let subject = await searchSubject({
        _id: values.radio
      })

      checked_sudent_list.forEach(async (item) => {
          let user = await getSingleUserInfoRequest(item._id)
          let topic_role = user.topic_role
          if(!topic_role.includes(values.radio)) {
              topic_role.push(values.radio)
              await userInfoPatch(item._id, {
                  topic_role
              })
          }

          await examPost({
            topic_list: check_list,
            two_id: values.radio,
            user_id: item._id,
            start_time: new Date(values.exam_time[0]),
            end_time: new Date(values.exam_time[1]),
            is_submit: false,
            total_score: total_score,
          })
      })

      await timeCreate({
        title: subject.data[0].two_name,
        subject_id: values.radio,
        start_time: new Date(values.exam_time[0]),
        end_time: new Date(values.exam_time[1]),
        user_list: checked_sudent_list.map((item: any) => item._id),
        topic_list: check_list,
        total_score: total_score
      })

      dispatch(set_is_show_user_edit_modal(false))
      dispatch(set_checked_sudent_list([]))
      message.success('操作成功')
      navigate('/exam_manage')
    };

    // 获取课程下的所有题目
    async function SelectSubject(e: any) {
      setTotalScore(0)
      let list = await getTopic2List(e.target.value)
      setTopicList(list)
      let topic_checkbox =  list.map((item: any) => {
        return {
          label: item.title + '（' +  item.score + '分）',
          value: item._id
        }
      })
      setCheckboxOptions(topic_checkbox)
    }

    // 选择题目
    async function SelectTopic(value: any) {
      let list: any[] = []
      for (const item of value) {
        let index = topic_list.findIndex((topic_item: any) => topic_item._id === item)
        list.push(topic_list[index])
      }
      setCheckList(list)
    }

    return (
      <Modal
          title="编辑考试权限"
          open={is_show}
          footer={null}
          onCancel={onCancel}
      >
          <Form
              name='sub_form'
              form={form}
              style={{marginTop: 20}}
              onFinish={onFinish}
          >
              <Form.Item label="选择课程" name="radio" rules={[{ required: true, message: '请选择课程' }]}>
                  <Radio.Group
                      options={radio_options}
                      onChange={SelectSubject}
                  />
              </Form.Item>
              {
                topic_list.length > 0 &&
                <>
                  <Form.Item label="选择考题" name="checkbox" style={{marginBottom: '10px'}} rules={[{ required: true, message: '请选择考题' }]}>
                    <Checkbox.Group
                        options={checkbox_options}
                        onChange={SelectTopic}
                    />
                  </Form.Item>
                  <div style={{marginBottom: '20px'}}>
                    当前试卷总分: {total_score}
                  </div>
                </>
              }
              <Form.Item label="考试时间" name="exam_time" rules={[{ required: true, message: '请选择考试时间' }]}>
                  <DatePicker.RangePicker showTime format="YYYY-MM-DD HH:mm:ss" />
              </Form.Item>
              <Form.Item>
                  <Button type="primary" htmlType='submit'>确定</Button>
              </Form.Item>
          </Form>
      </Modal>
    )
}

export default EditModal;