import { Button, Form, Input, Radio } from 'antd';
import { useAppDispatch, AppDispatch, useAppSelector } from '../../store/index';
import { get_student_async, set_student_list_current_page, set_student_list_search_params, set_is_show_user_edit_modal, select_checked_sudent_list } from '../../store/slice/user';
import styles from './index.module.css'

function Search() {
    const [form] = Form.useForm();
    const disptch: AppDispatch = useAppDispatch()

    const checked_sudent_list = useAppSelector(select_checked_sudent_list)

    async function search_click() {
        const form_data = await form.validateFields()

        Object.keys(form_data).forEach((key: string) => {
            if (!form_data[key]) {
                delete form_data[key]
            }
        })
        
        disptch(set_student_list_search_params(form_data))
        disptch(set_student_list_current_page(1))
        disptch(get_student_async(form_data))
    }

    async function edit_click(record: any) {
        disptch(set_is_show_user_edit_modal(true))
    }

    return (
        <div className={styles.wrap}>
            <Form
                layout='inline'
                form={form}
            >
                <Form.Item label="用户名" name="name">
                    <Input placeholder="请输入" />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" onClick={search_click}>查询</Button>
                </Form.Item>
            </Form>
            <Button disabled={checked_sudent_list.length ? false : true} type='primary' onClick={edit_click}>编辑考试权限</Button>
        </div>
    )
}

export default Search;