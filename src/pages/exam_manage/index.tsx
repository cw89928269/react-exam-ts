import { useAppDispatch, useAppSelector } from '@/store';
import { Pagination, Table, Tag, message } from 'antd';
import dayjs from 'dayjs';
import { useEffect } from 'react';
import { get_exam_history, get_time_history, select_exam_history_data, select_time_history_data, select_time_list_loading, set_cur_exam_user_list, set_cur_time_id, set_is_show_time_edit_modal, set_time_area, set_time_list_data, set_is_show_rank_modal, set_cur_subject_title } from '../../store/slice/subject';
import { examDelete, getExamHistory, getSingleUserInfoRequest, timeDelete, userInfoPatch } from '../../util/request';
import Modal from './Modal';
import RankModal from './RankModal';
import styles from './index.module.css';

const PAGE_COUNT = 10;

function ExamManage() {
    const dispatch = useAppDispatch()

    let time_list_data = useAppSelector(select_time_history_data)

    const loading = useAppSelector(select_time_list_loading)

    let time_list = time_list_data.list?.map((item: any) => {
        return {
            ...item,
            key: item._id
        }
    })

    useEffect(() => {
        dispatch(get_exam_history({}))
        dispatch(get_time_history({}))
    }, [])

    function show_click(item: any) {
        dispatch(set_is_show_rank_modal(true))
        dispatch(set_time_area([item.start_time, item.end_time]))
        dispatch(set_cur_subject_title(item.title))
    }

    async function change_click(item: any) {
        dispatch(set_is_show_time_edit_modal(true))
        dispatch(set_time_area([item.start_time, item.end_time]))
        dispatch(set_cur_time_id(item._id))
        dispatch(set_cur_exam_user_list(item.user_list))
    }

    async function delete_click(item: any) {
        let res: any
        await timeDelete(item._id)
        res = await getExamHistory({})
        let delete_list = res.data.map((ex: any) => {
          if(ex.subject_name === item.title && ex.start_time === item.start_time) {
            return ex._id
          } 
        })

        delete_list.forEach(async (item: any) => {
          await examDelete(item)  
        })

        res = await getExamHistory({})

        item.user_list.forEach(async (user_id: any) => {
          let user = await getSingleUserInfoRequest(user_id)
          let exists = res.data.some((ex: any) => ex.subject_name === item.title && user.username === ex.user_name)
          if(!exists) {
            let rest_role: any = user.topic_role.filter((ex: any) => {
              return ex !== item.subject_id
            })
            await userInfoPatch(user_id, {
              topic_role: rest_role
            })
          }
        })

        dispatch(get_time_history({}))
        message.success('删除成功')
    }

    const tableColumns = [{
        title: '试卷名称',
        dataIndex: 'title',
        key: 'title',
    }, {
        title: '考试时间',
        dataIndex: 'exam_time',
        key: 'exam_time',
        render: (_: any, record: any) => {
            return (
              <>
                <span>{ dayjs(record.start_time).format('YYYY MM-DD HH:mm:ss') + " - " }</span>
                <span>{ dayjs(record.end_time).format('YYYY MM-DD HH:mm:ss') }</span>
              </>
            )
        }
    }, {
        title: '操作',
        dataIndex: '_id',
        key: '_id',
        render: (row: any, record: any) => {
            return (
              <>
                <Tag color={'green'} onClick={() => {
                    show_click(record)
                }} style={{ cursor: 'pointer' }}>
                    查看排名
                </Tag>
                <Tag color={'blue'} onClick={() => {
                    change_click(record)
                }} style={{ cursor: 'pointer' }}>
                    编辑
                </Tag>
                <Tag color={'red'} onClick={() => {
                    delete_click(record)
                }} style={{ cursor: 'pointer' }}>
                    删除
                </Tag>
              </>
            )
        },
    }]

    function page_change(count: number) {
        dispatch(set_time_list_data({
            current_page: count
        }))

        dispatch(get_time_history({
            ...time_list_data.search_params,
            skip: PAGE_COUNT * (count - 1)
        }))
    }

    return (
        <>
            <div className={styles["exam-history"]}>
                <div className='table-list-wrapper'>
                    <Table loading={loading} dataSource={time_list} columns={tableColumns} pagination={false} rowKey="_id" />
                    <Pagination
                        className={styles.pagenation}
                        pageSize={PAGE_COUNT}
                        current={time_list_data.current_page}
                        total={time_list_data.count}
                        onChange={page_change}
                    />
                </div>
            </div>
            <Modal></Modal>
            <RankModal></RankModal>
        </>

    )
}

export default ExamManage