import { useAppDispatch } from '@/store';
import { useAppSelector } from '@/store/index';
import { corretExamPost, examDelete, examFind, examPost, getExamHistory, getSingleUserInfoRequest, getTopic2List, timeSingleGet, timeUpdate, userInfoPatch } from '@/util/request';
import { Button, Checkbox, DatePicker, Form, Modal, message } from 'antd';
import dayjs from 'dayjs';
import { useEffect } from 'react';
import { get_time_history, select_cur_exam_user_list, select_cur_time_id, select_exam_history_data, select_is_show_user_edit_modal, select_time_area, set_cur_exam_user_list, set_is_show_time_edit_modal, set_time_area } from '../../store/slice/subject';
import { get_student_async, select_user_student_list_data } from '../../store/slice/user';
import { CurTime } from './data';

function EditModal() {
    const dispatch = useAppDispatch()

    const area = useAppSelector(select_time_area)
    
    const user_list = useAppSelector(select_cur_exam_user_list)

    // 学生列表
    let checkbox_options: any = []
    const data: any = useAppSelector(select_user_student_list_data)
    data.list?.forEach((item: any) => {
        checkbox_options.push({
            label: item.username,
            value: item._id
        })
    })

    const [form] = Form.useForm();

    const is_show = useAppSelector(select_is_show_user_edit_modal)

    const time_id = useAppSelector(select_cur_time_id)

    useEffect(() => {
      form.setFieldValue('checkbox', user_list)  
    },[user_list])

    useEffect(() => {
        dispatch(get_student_async({}))
    }, [])

    const onCancel = () => {
        dispatch(set_is_show_time_edit_modal(false))
    };

    const stuOnChange = (value: any) => {
        dispatch(set_cur_exam_user_list(value))
    }

    const timeOnChange = (value: any) => {
        dispatch(set_time_area([new Date(value[0]), new Date(value[1])]))
    }

    const onFinish = async() => {
      // 用户列表遍历 和 选择列表作比较
        data.list.forEach(async (item: any) => {
          let user = await getSingleUserInfoRequest(item._id)
          let cur_time: CurTime = await timeSingleGet(time_id)

          let exam_find_list: any = await examFind({
            user_id: item._id,
            start_time: cur_time.start_time
          })
          let find = exam_find_list[0]
          
          // 如果复选框不包含该用户
          if(!user_list.includes(item._id)) {
            if(find) {
              await examDelete(find._id)

              let now_exam_list: any = await getExamHistory({})
              let exists = now_exam_list.data.some(
                (ex: any) => 
                  cur_time.start_time !== ex.start_time && cur_time.title === ex.subject_name
              )
              if(!exists) {
                let rest_list: any = user.topic_role.filter((item: any) => item !== cur_time.subject_id)
                await userInfoPatch(user._id, {
                  topic_role: rest_list
                })
              }
            }
          } else {
            if(!find) {
              await examPost({
                topic_list: cur_time.topic_list,
                two_id: cur_time.subject_id,
                user_id: item._id,
                start_time: new Date(area[0]),
                end_time: new Date(area[1]),
                is_submit: false,
                total_score: cur_time.total_score
              })
              if(!user.topic_role.includes(cur_time.subject_id)) {
                let new_list = [...user.topic_role, cur_time.subject_id]
                await userInfoPatch(user._id, {
                  topic_role: new_list
                })
              }
            } else {
              await corretExamPost(find._id, {
                start_time: new Date(area[0]),
                end_time: new Date(area[1]),
              })
            }
          }

        })

        await timeUpdate(time_id, {
            user_list,
            start_time: area[0],
            end_time: area[1]
        })
        
        dispatch(get_time_history({}))
        dispatch(set_is_show_time_edit_modal(false))
        message.success('更新成功')
    };

    return (
      <Modal
          title="编辑考试信息"
          open={is_show}
          footer={null}
          onCancel={onCancel}
          forceRender
      >
          <Form
              name='sub_form'
              form={form}
              style={{marginTop: 20}}
              onFinish={onFinish}
          >
            <Form.Item label="学生列表" name="checkbox" initialValue={user_list}>
                <Checkbox.Group
                    options={checkbox_options}
                    onChange={stuOnChange}
                />
            </Form.Item>
            <Form.Item label="考试时间" name="exam_time">
                <DatePicker.RangePicker showTime format="YYYY-MM-DD HH:mm:ss" defaultValue={[dayjs(area[0]), dayjs(area[1])]} onChange={timeOnChange}  />
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType='submit'>确定</Button>
            </Form.Item>
          </Form>
      </Modal>
    )
}

export default EditModal;