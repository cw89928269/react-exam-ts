import { useAppDispatch } from '@/store';
import { useAppSelector } from '@/store/index';
import { get_exam_history, select_cur_subject_title, select_exam_history_data, select_is_show_rank_modal, select_time_area, set_is_show_rank_modal } from "@/store/slice/subject";
import { Modal, Table } from "antd";
import { useEffect } from "react";

function RankModal() {
  const dispatch = useAppDispatch()
  const is_show = useAppSelector(select_is_show_rank_modal)
  const exam_list_data: any = useAppSelector(select_exam_history_data)
  const area = useAppSelector(select_time_area)
  const cur_subject_title = useAppSelector(select_cur_subject_title)
  const filter_list = exam_list_data.list?.filter((item: any) => {
    return item.subject_name === cur_subject_title && item.start_time === area[0] && item.get_score !== undefined
  })
  const rank_list = filter_list.length !== 0 ? filter_list.sort((a: any,b: any) => b.get_score - a.get_score).map((obj: any, index: any) => ({ ...obj, rank: index + 1, key: index + 1 })) : []

  useEffect(() => {
    dispatch(get_exam_history({}))
  }, [])

  const tableColumns = [{
    title: '用户名',
    dataIndex: 'user_name',
    key: 'user_name',
  },  {
    title: '分数',
    dataIndex: 'get_score',
    key: 'score'
  }, {
    title: '排名',
    dataIndex: 'rank',
    key: 'rank' 
  }]

  const onCancel = () => {
    dispatch(set_is_show_rank_modal(false))
  };

  return (
    <Modal
      title="考试排名"
      open={is_show}
      onCancel={onCancel}
      footer={null}
      width="60%"
    >
      <Table dataSource={rank_list} columns={tableColumns} pagination={false} ></Table>
    </Modal>
  )
}

export default RankModal;