export type CurTime = {
  _id: string,
  created: Date,
  end_time: string,
  start_time: string,
  subject_id: string,
  title: string,
  user_list: string[],
  topic_list: any[],
  total_score: number
}