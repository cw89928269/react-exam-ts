import { useEffect, useState } from "react";
import styles from "./index.module.css";
import { Button, Divider, Input, Empty } from 'antd';
import { useParams, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from '@/store';
import { select_exam_topic_list, set_current_exam_topic_id, select_current_exam_topic, get_corret_exam_async, set_exam_corret, set_exam_type } from '@/store/slice/subject';
import { corretExamPost } from '@/util/request';
import TopicCp from '../../common_components/topic/index';


function CorretExam() {
    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const [score, set_score] = useState('')
    const [can_submit, set_can_submit] = useState(false)
    const params = useParams()

    // 题目列表
    const topic_list: any[] = useAppSelector(select_exam_topic_list)

    const short_list: any[] = topic_list.filter((item) => {
       return item.topic_type === 'short'
    })
    
    // 当前选中的题目对象
    const current_exam_topic: any = useAppSelector(select_current_exam_topic)

    useEffect(() => {
        let flag = false
        flag = short_list.every((item) => {
            return item.is_corret
        })
        set_can_submit(flag)
    }, [topic_list])


    useEffect(() => {
        const exam_id: any = params.exam_id
        dispatch(get_corret_exam_async(exam_id))
        dispatch(set_exam_type('corret'))
    }, [])

    function topic_click(item: any) {
        if (item._id !== current_exam_topic._id) {
          set_score('')
        }
        dispatch(set_current_exam_topic_id(item._id))
    }

    function pass(data: any) {
        dispatch(set_exam_corret(data))
    }

    async function submit_click() {
        await corretExamPost(params.exam_id as string, {
            topic_list,
            is_judge: true,
            status: 'finish'
        })
        navigate('/corret_exam_list')
    }
    
    return (
        <div>
            <div className={styles.exam}>
                {
                    short_list.length > 0 ? (
                        <div className={styles.exam_left}>
                        <div className={styles.title}> 考题列表</div>
                        <div className={styles.exam_left_content}></div>
                        {short_list.map((item, index) => {
                            return (
                                <div
                                    key={index}
                                    className={`${styles.questiontab}`}
                                    onClick={() => {
                                        topic_click(item)
                                    }}
                                >
                                    <div
                                        className={`${styles.question} ${current_exam_topic._id === item._id ? styles.alreadyselect : ""
                                            }`}
                                    >
                                        {item.title}
                                    </div>
                                    <div
                                        className={`${styles.circle}  ${ item.is_corret ? styles.alreadykeep : ""
                                        }`}
                                    ></div>
                                </div>
                            );
                        })}
                    </div>
                    ) : null
                }

                {
                  Object.keys(current_exam_topic).length > 0 ? (
                      <div className={styles.exam_right}>
                          <TopicCp
                              type='corret'
                              topic={current_exam_topic}
                              score_cb={pass}
                          />
                          {
                              short_list[short_list.length - 1] === current_exam_topic &&
                              <Button
                                  type="primary"
                                  className={styles.summitbtn}
                                  disabled={!can_submit}
                                  onClick={submit_click}
                              >
                                提交阅卷
                              </Button>
                          }
                      </div>
                  ) : null
                }
                
            </div>
        </div>
    );
}

export default CorretExam;
