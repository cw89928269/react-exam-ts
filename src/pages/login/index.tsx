import React, { useEffect, useState } from 'react'
import { Button, Form, Input, message } from 'antd'
import style from './index.module.scss'
import login_desc from './assets/login_desc.png'
import { useNavigate } from 'react-router'
import { useAppDispatch } from '@/store/index';
import { set_user_info } from '@/store/slice/user';
import { loginPost, LoginBody } from '@/util/request';

const LoginPage: React.FC = () => {
	const [count, set_count] = useState(0)
	const [show, set_show] = useState(true)
	const [form] = Form.useForm()
	const navigate = useNavigate()
	const dispatch = useAppDispatch()

	const onFinishFailed = (errorInfo: any) => {

	}

	const register = () => {
		navigate('/register',{replace:true})
	}

	async function onLogin(value: LoginBody) {
		// @ts-ignore
		const res = await loginPost(value)
		if(res.code === 1) {
			message.error(res.msg)
		} else {
			const user_info = res.data
			dispatch(set_user_info(user_info))

			if(!user_info.has_person_info && user_info.role === 'student') {
				navigate('/person_info')
			} else {
				if(user_info.role === 'student') {
					navigate('/exam_select')
				}

				if(user_info.role === 'admin') {
					navigate('/corret_exam_list')
				}

				if(user_info.role === 'super_admin') {
					navigate('/corret_exam_list')
				}
			}
		}
	}

	return (
		<div className={style.page_container}>

			<div className={style.login_container}>
				<div className={style.login_left}>
					<div className={style.left_title}>
						<img src={login_desc} alt="" />
					</div>
				</div>
				<div className={style.login_right}>
					<div className={style.right_form}>
						<Form onFinish={onLogin} size="large" labelCol={{ span: 5 }} wrapperCol={{ span: 20 }}  labelAlign="left" onFinishFailed={onFinishFailed} form={form}>
							<Form.Item
								label="用户名"
								name="username"
								rules={[
									{ required: true, message: '请填写用户名' },
								]}
							>
								<Input placeholder="请输入用户名" />
							</Form.Item>
							<div style={{ position: 'relative' }}>
								<Form.Item label="密码" name="password" rules={[{ required: true, message: '请输入密码' }]}>
									<Input type='password' placeholder="请输入密码" style={{ padding: '7px 100px 7px 11px' }} />
								</Form.Item>
							</div>
							<div className={style.form_btn}>
								<Button type="primary" htmlType="submit">
									登录
								</Button>
							</div>
						</Form>
					</div>
					<div className={style.right_register} onClick={register}>
						<span>注册</span>
					</div>
				</div>
			</div>

		</div>
	)
}

export default LoginPage
