export type TimerType = {
  hours?: string; // 小时
  minutes?: string; // 分钟
  seconds?: string; // 秒
} 