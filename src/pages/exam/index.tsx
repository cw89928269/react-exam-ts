import TopicCp from "@/common_components/topic";
import { useAppDispatch, useAppSelector } from '@/store';
import { Button } from 'antd';
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { get_exam_async, select_curr_exam, select_current_exam_topic, select_exam_topic_list, select_rest_time, set_current_exam_topic_id, set_exam_answer, set_exam_topic_list, set_exam_type, set_rest_time } from '../../store/slice/subject';
import { select_user_info } from '../../store/slice/user';
import { corretExamPost, getExamHistory, userInfoPatch } from '../../util/request';
import styles from "./index.module.css";

function Exam() {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [answer, set_answer] = useState('')

  const [can_submit, set_can_submit] = useState(false)
  const params: any = useParams()

  const [currentTime, setCurrentTime] = useState(new Date());

  // 题目列表
  const topic_list: any[] = useAppSelector(select_exam_topic_list)
  
  // 当前选中的题目id
  const current_exam_topic: any = useAppSelector(select_current_exam_topic)

  const curr_exam: any = useAppSelector(select_curr_exam)
  const end_time: any = curr_exam.end_time

  const user_info: any = useAppSelector(select_user_info)

  const rest_time: any = useAppSelector(select_rest_time)

  useEffect(() => {
    if(topic_list.length !== 0) {
      submit_answer_click()
    }
    let flag = false
    flag = topic_list.every((item) => {
      if(item.topic_type === 'multiple' && item.answer.length === 0) {
        return false
      }
      return item.answer
    })
    set_can_submit(flag)
  }, [topic_list])

  useEffect(() => {
    const timerId = setInterval(async () => {
      if(typeof rest_time === 'number' && rest_time <= 0) {
        clearInterval(timerId)
        submit_click()
        dispatch(set_rest_time(''))
        return;
      }
      dispatch(set_rest_time((new Date(end_time)).getTime() - (new Date()).getTime()))
    }, 1000)
    return () => clearInterval(timerId)
  })

  useEffect(() => {
    dispatch(get_exam_async(params.exam_id))
    dispatch(set_exam_type('exam'))
  }, [])
  
  function topic_click(item: any) {
    if (item._id !== current_exam_topic._id) {
      set_answer('')
    }
    dispatch(set_current_exam_topic_id(item._id))
  }

  async function submit_answer(data: any) {
    dispatch(set_exam_answer(data))
  }

  async function submit_answer_click() {
    await corretExamPost(params.exam_id as string, {
      topic_list
    })
  }

  async function submit_click() {
    await corretExamPost(params.exam_id as string, {
      topic_list,
      is_submit: true,
      status: 'submit'
    })

    let res: any = await getExamHistory({})
    let exists = res.data.some((item: any) => item.two_id === curr_exam.two_id && item.end_time > curr_exam.end_time)
    if(!exists) {
      let rest_list: any = user_info.topic_role.filter((item: any) => {
        return item !== current_exam_topic.two_id
      })
      await userInfoPatch(user_info._id, {
        topic_role: rest_list
      })
    }
    dispatch(set_exam_topic_list([]))
    navigate('/exam_history')
  }

  return (
    <div className={styles.exam}>
      {
        topic_list.length > 0 ? (
          <div className={styles.exam_left}>
            <div className={styles.title}> 考题列表</div>
            <div className={styles.exam_left_content}></div>
            {topic_list.map((item, index) => {
              return (
                <div
                  key={index}
                  className={`${styles.questiontab}`}
                  onClick={() => {
                    topic_click(item)
                  }}
                >
                  <div
                    className={`${styles.question} ${current_exam_topic._id === item._id ? styles.alreadyselect : "" }`}
                  >
                    {item.title}
                  </div>
                  <div
                    className={`${styles.circle}  ${ item.answer && item.is_take || (item.topic_type === 'short' && item.is_corret) ? styles.alreadykeep : "" }`}
                  ></div>
                </div>
              );
            })}
          </div>
        ) : null
      }
      {
        Object.keys(current_exam_topic).length > 0 ? (
          <div className={styles.exam_right}>
            <TopicCp
              type="exam"
              topic={current_exam_topic}
              answer_cb={submit_answer}
            />
            {
              topic_list[topic_list.length - 1] === current_exam_topic && (
                <Button
                  type="primary"
                  className={styles.summitbtn}
                  disabled={!can_submit}
                  onClick={submit_click}
                >
                  点击交卷
                </Button>
              )
            }
          </div>
        ) : null
      }

      {
        (typeof rest_time === 'number' && rest_time >= 0) ? (
          <div className={styles.modal}>
            {
              "剩余时间: " + new Date(rest_time).getUTCHours() + " 小时 " + new Date(rest_time).getUTCMinutes() + " 分钟 " + new Date(rest_time).getUTCSeconds() + " 秒"
            }
          </div>
        ) : null
      }

    </div>
  );
}

export default Exam;
