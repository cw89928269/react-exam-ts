import { useAppSelector } from '@/store';
import { AppDispatch } from "@/store/index";
import {
  get_subject_tree_async, set_current_two_subject,
} from "@/store/slice/subject";
import { examFind, userInfoPatch } from '@/util/request';
import { message } from 'antd';
import classnames from "classnames";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { select_current_two_subject, select_subject_tree } from '../../store/slice/subject';
import { select_user_info } from '../../store/slice/user';
import styles from "./index.module.scss";

function ExamSelect() {
  const location = useLocation()
  const data = useAppSelector(select_subject_tree)

  const dispatch: AppDispatch = useDispatch();
  const navigate = useNavigate();

  const current_two_subject = useAppSelector(select_current_two_subject)
  const user_info = useAppSelector(select_user_info)

  useEffect(() => {
    dispatch(get_subject_tree_async());
    return () => {
      dispatch(set_current_two_subject(""))
    }
  }, []);

  async function handleJump () {
    const now = new Date()
    if (!current_two_subject) {
      message.warning('请选择题目再作答')
    } else {
      const res: any = await examFind({
        two_id: current_two_subject,
        user_id: user_info._id,
      })

      let index = res.findIndex((item: any) => 
        ((now >= new Date(item.start_time) && now <= new Date(item.end_time)) && item.is_submit === false) || ((new Date(item.start_time) >= now  && item.is_submit === false))
      )

      if(new Date(res[index].start_time) > now) {
        message.warning('当前时间无法作答')
      } else {
        navigate({
          pathname: `/exam/${res[index]._id}`,
        });
      }

    }
  };

  function item_click(item: any) {
    if(!item.can_exam) {
      return
    }
    dispatch(set_current_two_subject({cur_sub: item.value, is_exam: true}))
  }

  return (
    <>
      <div className={styles.wrap}>
        <div className={styles.content}>
          <div>
            {data?.map((item, index) => (
              <React.Fragment key={item.title}>
                <div
                  style={{
                    fontFamily: "Source Han Sans CN",
                    fontWeight: "bold",
                    fontSize: "22px"
                  }}
                  className={styles.title}
                >
                  <div>{item.title}</div>
                </div>
                <div className={styles.topic_section}>
                  {item?.children.map(
                    (_item) => (
                      <div
                        key={_item.value}
                        onClick={() => {
                          item_click(_item)
                        }}

                        className={classnames(styles.topic_section_content, {
                          topic_section_content_selected:
                            _item.value === current_two_subject,
                          topic_section_content_disabled:
                            _item.can_exam === false,
                        })}
                      >
                        <p>{_item.title}</p>
                      </div>
                    )
                  )}
                </div>
              </React.Fragment>
            ))}
          </div>
        </div>
        <div className={styles.footer}>
          <button onClick={handleJump}>开始答题</button>
        </div>
      </div>
    </>
  );
}

export default ExamSelect;
