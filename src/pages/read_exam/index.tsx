import { useEffect } from "react";
import styles from "./read_exam.module.css";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from '@/store';
import { select_exam_topic_list, set_exam_topic_list, set_exam_answer, get_corret_exam_async, select_curr_exam } from '../../store/slice/subject';
import TopicCp from "@/common_components/topic";

function ReadExam() {
    const dispatch = useAppDispatch()
    const params = useParams()
    const exam = useAppSelector(select_curr_exam)
    // 题目列表
    const topic_list: any[] = useAppSelector(select_exam_topic_list)

    useEffect(() => {
        const exam_id: any = params.exam_id
        dispatch(get_corret_exam_async(exam_id))
        return () => {
            dispatch(set_exam_topic_list([]))
        }
    }, [])

    return (
      <div>
        {
          topic_list.length > 0 ? (
            <div className={styles.exam}>
              <div className={styles.exam_left}>
                  <div className={styles.title}> 考题列表</div>
                  <div className={styles.exam_left_content}></div>
                  {topic_list.map((item, index) => {
                      return (
                          <div
                              key={index}
                              className={`${styles.questiontab}`}
                          >
                              <div
                                  className={`${styles.question}`}
                              >
                                  {item.title}
                              </div>
                              {
                                  item.correct ?
                                      (<div
                                          className={`${styles.circle} ${styles.alreadykeep}`}
                                      ></div>) : null
                              }
                              {
                                  !item.correct ?
                                      (<div
                                          className={`${styles.wrong} ${styles.no_pass}`}
                                      ></div>) : null
                              }
                          </div>
                      );
                  })}
              </div>
                <div className={styles.exam_right}>
                    {
                        <div className={styles.total_title}>
                            得分 <span style={{color: 'red'}}>{exam.get_score}</span> /
                            总分 <span style={{color: 'red'}}>{exam.total_score}</span>
                        </div>
                    }
                    {
                        topic_list.map((item) => {
                            return (
                                <div className={styles.topic_wrap}>
                                    <TopicCp
                                        type="read"
                                        topic={item}
                                    />
                                </div>
                            )
                        })
                    }
                </div>
            </div>
          ) : null
        }
      </div>
    )
}

export default ReadExam