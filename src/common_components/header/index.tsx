import { useAppSelector } from '@/store'
import { select_user_info } from '@/store/slice/user'
import { getImgUrl } from '@/util'
import { Avatar, Button, Dropdown, Typography } from 'antd'
import { useNavigate } from 'react-router-dom'
import { logoutRequest } from '../../util/request'
import avatar from './assets/avatar.png'
import logo from './assets/logo.png'
import styles from './index.module.scss'

const { Text } = Typography

export default function Header() {
	const navigate = useNavigate()

	const userInfo = useAppSelector(select_user_info)

	async function logout() {
		await logoutRequest()
		navigate('/login')
	}

	return (
		<div className={styles.wrap}>
			<div className="logo">
				<img src={logo} alt="考试测评系统" />
			</div>
			<div className="info">
				<div className="profile">
					<Dropdown
						placement="bottomRight"
						arrow
						menu={{
							items: [
								{
									key: 'user',
									label: (
										<Button style={{ color: 'unset' }} type="link" onClick={() => navigate('/person_info')}>
											个人中心
										</Button>
									),
								},
								{
									key: 'logout',
									label: (
										<Button
											style={{ color: 'unset' }}
											type="link"
											onClick={logout}
										>
											退出登录
										</Button>
									),
								},
							],
						}}
					>
						<Avatar className="avatar" size={44} src={getImgUrl(userInfo?.avatar) || avatar} draggable={false} />
					</Dropdown>

					<div className="name">
						<Text style={{ width: '100px' }} ellipsis={{ tooltip: userInfo?.name }}>
							{userInfo?.username}
						</Text>
					</div>
				</div>
			</div>
		</div>
	)
}
