import { useAppDispatch } from '@/store';
import { Button, Checkbox, Input, Radio, RadioChangeEvent } from 'antd';
import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import { set_current_exam_topic_id } from '../../store/slice/subject';
import { TopicData } from '../../util/request';
import styles from './index.module.scss';

type Iprops = { // 考试 批阅 查看
    type: 'exam' | 'read' | 'corret'
    topic: TopicData
    score_cb?: any,
    answer_cb?: any,
}

const checkOptions = [
  { label: 'A', value: 'A' },
  { label: 'B', value: 'B' },
  { label: 'C', value: 'C' },
  { label: 'D', value: 'D' }
]

const TopicCp: React.FC<Iprops> = (props) => {
  const dispatch = useAppDispatch()
  const params: any = useParams()
    // 学生
    const [single, setSingle] = useState('');
    const [multiple, setMultiple] = useState([]);
    const [answer, set_answer] = useState('');

    // 教师
    const [score, set_score] = useState('')
    const [corret, set_corret] = useState('')

    function corret_change(e: any) {
      set_corret(e.target.value)
    }

    function answer_change(e: any) {
      set_answer(e.target.value)
    }

    useEffect(() => {
      return () => {
        dispatch(set_current_exam_topic_id(""))
      }
    }, [])

    useEffect(() => {
        set_corret(props.topic.comment)
    }, [props.topic.comment])

    useEffect(() => {
      if(props.topic.give_score) {
        set_score(props.topic.give_score)
      } else {
        set_score('')
      }
    }, [props.topic.give_score])

    useEffect(() => {
      if(props.topic.topic_type === 'select' || props.topic.topic_type === 'judge') {
        setSingle(props.topic.answer)
      } else if (props.topic.topic_type === 'multiple') {
        setMultiple(props.topic.answer)
      } else {
        set_answer(props.topic.answer)
      }
    }, [props.topic.answer])

    // 打分/评价 变动时设置
    useEffect(() => {
        if(props.score_cb) {
          if(score === '' || Number(score) > Number(props.topic.score) || Number(score) < 0) {
            props.score_cb({
              _id: props.topic._id,
              give_score: score,
              is_corret: false,
              comment: corret
            })
          } else {
            props.score_cb({
              _id: props.topic._id,
              give_score: score,
              is_corret: true,
              comment: corret
            })
          }
        }
    }, [score, corret])

    function scoreChange(e: any) {
      set_score(e.target.value)
    }

    async function submit_answer() {
      if (props.topic.topic_type === 'select' || props.topic.topic_type === 'judge') {
        props.answer_cb({
          answer: single,
          is_take: true,
          _id: props.topic._id,
        })
      } else if (props.topic.topic_type === 'multiple') {
        props.answer_cb({
          answer: multiple,
          is_take: true,
          _id: props.topic._id,
        })
      } else {
        props.answer_cb({
          answer: answer ? answer : '',
          is_take: true,
          _id: props.topic._id,
        })
      }
    }

    const onSingleChange = (e: RadioChangeEvent) => {
      setSingle(e.target.value)
    };

    const onMultipleChange = (multiple: any) => {
      setMultiple(multiple)
    };

    return (
      <div className={styles.wrap}>
            <div className={styles.title}>
              {
                props.topic.topic_type === 'select' ? `单选题(${props.topic.score}分)`: 
                ( props.topic.topic_type === 'multiple' ? `多选题(${props.topic.score}分)`: 
                  (props.topic.topic_type === 'short' ? `简答题(${props.topic.score}分)`: `判断题(${props.topic.score}分)`)
                )
              }
            </div>
            
            {/* 题干内容 */}
            <p className={styles.content}>
                {props.topic.title}
            </p>
            
            {/* 单选/多选 选项 + 选择 */}
            {
                 props.topic.topic_type === 'select' && (
                    <>
                      <div className={styles.title}>
                        选项
                      </div>
                      { 
                          props.topic.options.map((item: any, index: number) => {
                              return (
                                <div key={index}>
                                  <p className={styles.content}>
                                      A: {item.A}
                                  </p>
                                  <p className={styles.content}>
                                      B: {item.B}
                                  </p>
                                  <p className={styles.content}>
                                      C: {item.C}
                                  </p>
                                  <p className={styles.content}>
                                      D: {item.D}
                                  </p>
                                </div>
                              )
                          })
                      }
                      {
                        props.type === 'exam' && props.topic.topic_type === 'select' && (
                          <Radio.Group style={{marginBottom: '20px'}} onChange={onSingleChange} value={single}>
                            <Radio value="A">A</Radio>
                            <Radio value="B">B</Radio>
                            <Radio value="C">C</Radio>
                            <Radio value="D">D</Radio>
                          </Radio.Group>
                        )
                      }
                    </>
                )
            }

            {/* 多选题 选项 + 选择 */}
            {
              props.topic.topic_type === 'multiple' && (
                <>
                  <div className={styles.title}>
                    选项
                  </div>
                  { 
                      props.topic.options.map((item: any, index: number) => {
                          return (
                            <div key={index}>
                              <p className={styles.content}>
                                  A: {item.A}
                              </p>
                              <p className={styles.content}>
                                  B: {item.B}
                              </p>
                              <p className={styles.content}>
                                  C: {item.C}
                              </p>
                              <p className={styles.content}>
                                  D: {item.D}
                              </p>
                            </div>
                          )
                      })
                  }
                  {
                    props.type === 'exam' && props.topic.topic_type === 'multiple' ? (
                      <Checkbox.Group
                        options={checkOptions}
                        style={{marginBottom: '20px'}}
                        value={ multiple }
                        onChange={onMultipleChange}
                      />
                    ) : null
                  }
                </>
              )
            }

            {/* 判断 选项 + 选择 */}
            {
                props.topic.topic_type === 'judge' && (
                    <>
                      <div className={styles.title}>
                        选项
                      </div>
                      { 
                          props.topic.options.map((item: any, index: number) => {
                              return (
                                <div key={index}>
                                  <p className={styles.content}>
                                      A: {item.A}
                                  </p>
                                  <p className={styles.content}>
                                      B: {item.B}
                                  </p>
                                </div>
                              )
                          })
                      }
                      {
                        props.type === 'exam' && (
                          <Radio.Group style={{marginBottom: '20px'}} onChange={onSingleChange} value={single}>
                            <Radio value="A">A</Radio>
                            <Radio value="B">B</Radio>
                          </Radio.Group>
                        )
                      }
                    </>
                )
            }

            {/* 考试简答题作答 */}
            {
              props.type === 'exam' && props.topic.topic_type === 'short' ? (
                <>
                  <div className={styles.title}>
                    作答
                  </div>
                  <p className={styles.content}>
                      <Input.TextArea
                          value={answer}
                          rows={6}
                          placeholder="请作答"
                          className={styles.customInput}
                          disabled={props.type !== 'exam'}
                          onChange={answer_change}
                      />
                  </p>
                </>
              ) : null
            }

            {/* 考试保存作答 */}
            {
                props.type === 'exam' ?
                    (<Button
                        type="primary"
                        className={styles.answer_btn}
                        onClick={submit_answer}
                        size="large"
                    >
                        保存作答
                    </Button>) : null
            }

            {/* 单选/判断 我的答案 */}
            {
                 props.type == 'read' && (props.topic.topic_type === 'select' || props.topic.topic_type === 'judge' ) && (
                    <>
                      <div className={styles.title}>
                        我的答案
                      </div>
                      <div className={styles.my_content}>
                        <div className={styles.left}>
                          {props.topic.answer}
                        </div>
                        <div className={styles.right}>
                          得分: <span className={styles.red}>{props.topic.correct ? props.topic.score : '0'}</span>
                        </div>
                      </div>
                      <div className={styles.title}>
                        正确答案
                      </div>
                      <div className={styles.my_content}>
                        <div className={styles.left}>
                          {props.topic.correct_single_option}
                        </div>
                      </div>
                    </>
                )
            }

            {/* 多选 我的答案 */}
            {
                 props.type == 'read' && props.topic.topic_type === 'multiple' && (
                    <>
                      <div className={styles.title}>
                        我的答案
                      </div>
                      <div className={styles.my_content}>
                        <div className={styles.left}>
                          {props.topic.answer.map((item: any, index: number) => {
                            return <span key={index}>{item} </span>
                          })}
                        </div>
                        <div className={styles.right}>
                          得分: <span className={styles.red}>{props.topic.correct ? props.topic.score : '0'}</span>
                        </div>
                      </div>
                      <div className={styles.title}>
                        正确答案
                      </div>
                      <div className={styles.my_content}>
                        <div className={styles.left}>
                          {props.topic.correct_multiple_option.map((item: any, index: number) => {
                            return <span key={index}>{item} </span>
                          })}
                        </div>
                      </div>
                    </>
                )
            }

            {/* 题目解析 */}
            {
              props.type === 'corret' && props.topic.analysis !== '' ? (
                <>
                  <div className={styles.title}>
                    解析
                  </div>
                  <p className={styles.content}>
                    {props.topic.analysis}
                  </p>
                </>
              ) : null
            }

            {/* 批改简答题 我的答案 */}
            {
              props.type === 'corret' && props.topic.topic_type === 'short' ? (
                <>
                  <div className={styles.title}>
                    学生作答
                  </div>
                  <div className={styles.my_content} style={{marginBottom: '20px'}}>
                    <div className={styles.left}>
                      {props.topic.answer}
                    </div>
                  </div>
                </>
              ) : null
            }

            {/* 查看简答题 我的答案 */}
            {
              props.type === 'read' && props.topic.topic_type === 'short' ? (
                <>
                  <div className={styles.title}>
                    我的答案
                  </div>
                  <div className={styles.my_content} style={{marginBottom: '20px'}}>
                    <div className={styles.left}>
                      {props.topic.answer}
                    </div>
                    <div className={styles.right}>
                      得分: <span className={styles.red}>{props.topic.give_score}</span>
                    </div>
                  </div>
                </>
              ) : null
            }

            {/* 题目解析 */}
            {
              props.type === 'read' && props.topic.analysis !== '' ? (
                <>
                  <div className={styles.title}>
                    解析
                  </div>
                  <p className={styles.content}>
                    {props.topic.analysis}
                  </p>
                </>
              ) : null
            }

            {/* 简答题 打分 */}
            {
              props.type === 'corret' ?
                (
                  <>
                    <div className={styles.title}>
                      打分
                    </div>
                    <div className={styles.submit_wrap}>
                      <Input value={score} style={{width: '80px'}} onChange={scoreChange}></Input>
                    </div>
                    {
                      Number(score) < 0 && <div style={{fontSize: '12px', color: 'red', marginBottom: '20px'}}>打分不能小于0</div>
                    }
                    {
                      Number(score) > Number(props.topic.score) && <div style={{fontSize: '12px', color: 'red', marginBottom: '20px'}}>打分超过当前题目分值</div>
                    }
                  </>
                ) : null
            }


            {/* 简答题 评价 */}
            {
              props.type == 'corret' ?
                (
                    <>
                        <div className={styles.title}>
                            评价
                        </div>
                        <p className={styles.content}>
                            <Input.TextArea
                                value={corret}
                                rows={6}
                                className={styles.customInput}
                                onChange={corret_change}
                                disabled={props.type !== 'corret'}
                            />
                        </p></>
                ) : null
            }

            {/* 查看 评价 */}
            {
              props.type == 'read' && props.topic.comment ?
                (
                    <>
                        <div className={styles.title}>
                            评价
                        </div>
                        <p className={styles.content}>
                            <p className={styles.content}>
                              {props.topic.comment}
                            </p>
                        </p></>
                ) : null
            }

        </div>
    ) 
}

export default TopicCp