import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { RootState } from "../index";
import {
  getSubjectTree,
  getTopic2List,
  SubjectData,
  TopicData,
  getExamHistory,
  ExamData,
  getExamByIdRequest,
  ResData,
  getSubejctOne,
  timeGet,
} from "../../util/request";

// 题库仓库state类型
type SubjectState = {
  loading: boolean;
  // 课程树形数据
  subject_tree: SubjectData[];
  // 当前选择课程
  active_two: SubjectData;
  // 题目列表
  topic_two_list: TopicData[];
  // 当前选择题目
  active_topic: TopicData;
  current_two_subject: string; // 当前选择的考试科目
  curr_exam: ExamData;
  exam_topic_list: []; // 考试题目列表
  current_exam_topic_id: string;
  exam_list_data: {
    list: [];
    count: number;
    limit: number;
    skip: number;
    search_params: {};
    current_page: 1;
  };
  corret_exam_list_loading: boolean;
  time_list_loading: boolean;
  subject_one: any[];
  selected: string;
  exam_type: string;
  rest_time: string;
  time_list_data: {
    list: [];
    count: number;
    limit: number;
    skip: number;
    search_params: {};
    current_page: 1;
  };
  is_show_time_edit_modal: boolean;
  time_area: any[];
  cur_time_id: string;
  cur_subject_title: string;
  curr_exam_user_list: any[];
  is_show_rank_modal: boolean;
};

const initialState = {
  loading: false,
  subject_tree: [],
  active_two: {} as SubjectData,
  topic_two_list: [],
  active_topic: {} as TopicData,
  current_two_subject: "",
  curr_exam: {} as ExamData,
  exam_topic_list: [],
  current_exam_topic_id: "",
  corret_exam_list: [],
  corret_exam_list_loading: false,
  time_list_loading: false,
  exam_list_data: {
    // 分页查询
    list: [],
    count: 0,
    limit: 10,
    skip: 0,
    search_params: {},
    current_page: 1,
  },
  subject_one: [],
  selected: "select",
  exam_type: "exam",
  rest_time: "",
  time_list_data: {
    // 分页查询
    list: [],
    count: 0,
    limit: 10,
    skip: 0,
    search_params: {},
    current_page: 1,
  },
  is_show_time_edit_modal: false,
  time_area: [],
  cur_time_id: "",
  curr_exam_user_list: [],
  is_show_rank_modal: false,
  cur_subject_title: "",
} as SubjectState;

export const get_subject_tree_async = createAsyncThunk<SubjectData[]>(
  "get/subject_tree",
  async (action, state) => {
    return await getSubjectTree();
  }
);

export const get_topic_two_list = createAsyncThunk<TopicData[], string>(
  "get/topic_two_list",
  async (action, state) => {
    return await getTopic2List(action);
  }
);

export const get_subject_one = createAsyncThunk(
  "get/subject_one",
  async (action, state) => {
    return await getSubejctOne();
  }
);

// 获取考试题目
export const get_exam_async = createAsyncThunk<ExamData, string>(
  "get/exam_topic",
  async (action, state) => {
    return await getExamByIdRequest(action);
  }
);

export const get_exam_history = createAsyncThunk<ResData, any>(
  "get/exam_history",
  async (action, state) => {
    return await getExamHistory(action);
  }
);

export const get_corret_exam_async = createAsyncThunk<ExamData, string>(
  "get/get_corret_exam_async",
  async (action, state) => {
    return await getExamByIdRequest(action);
  }
);

export const get_time_history = createAsyncThunk<ResData, any>(
  "get/time_history",
  async (action, state) => {
    return await timeGet(action);
  }
);

export const subjectSlice = createSlice({
  name: "subject",
  initialState,
  reducers: {
    set_cur_time_id: (state, action) => {
      state.cur_time_id = action.payload;
    },
    set_cur_subject_title: (state, action) => {
      state.cur_subject_title = action.payload;
    },
    set_time_area: (state, action) => {
      state.time_area = action.payload;
    },
    set_is_show_time_edit_modal: (state, action) => {
      state.is_show_time_edit_modal = action.payload;
    },
    set_is_show_rank_modal: (state, action) => {
      state.is_show_rank_modal = action.payload;
    },
    set_exam_type: (state, action) => {
      state.exam_type = action.payload;
    },
    set_exam_topic_selected: (state, action) => {
      state.selected = action.payload;
    },
    set_subject_active_two: (state, action) => {
      state.active_two = action.payload;
    },
    set_subject_active_topic: (state, action) => {
      state.active_topic = action.payload;
    },
    set_current_two_subject: (state, action) => {
      state.current_two_subject = action.payload.cur_sub;
    },
    set_exam_topic_list: (state, action) => {
      state.exam_topic_list = action.payload;
    },
    set_current_exam_topic_id: (state, action) => {
      state.current_exam_topic_id = action.payload;
    },
    set_exam_answer: (state, action) => {
      state.exam_topic_list.forEach((item: any) => {
        if (item._id === action.payload._id) {
          item.answer = action.payload.answer;
          item.is_take = action.payload.is_take;
        }
      });
    },
    set_exam_corret: (state, action) => {
      state.exam_topic_list.forEach((item: any) => {
        if (item._id === action.payload._id) {
          item.is_corret = action.payload.is_corret;
          item.give_score = action.payload.give_score;
          item.comment = action.payload.comment;
        }
      });
    },
    set_exam_list_data: (state, action) => {
      state.exam_list_data = {
        ...state.exam_list_data,
        ...action.payload,
      };
    },
    set_time_list_data: (state, action) => {
      state.time_list_data = {
        ...state.exam_list_data,
        ...action.payload,
      };
    },
    set_rest_time: (state, action) => {
      state.rest_time = action.payload;
    },
    set_cur_exam_user_list: (state, action) => {
      state.curr_exam_user_list = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      // 获取课程列表fulfilled
      .addCase(get_subject_tree_async.fulfilled, (state, res) => {
        state.subject_tree = res.payload;
      })
      // 获取题目列表pending
      .addCase(get_topic_two_list.pending, (state) => {
        state.loading = true;
      })
      // 获取题目列表fulfilled
      .addCase(get_topic_two_list.fulfilled, (state, res) => {
        state.topic_two_list = res.payload;
        state.loading = false;
      })
      .addCase(get_exam_async.fulfilled, (state, res: any) => {
        state.curr_exam = res.payload;
        state.exam_topic_list = res.payload.topic_list;
        state.current_exam_topic_id = res.payload.topic_list[0]._id;
      })
      .addCase(get_exam_history.pending, (state, res: any) => {
        state.corret_exam_list_loading = true;
      })
      .addCase(get_exam_history.fulfilled, (state, res: any) => {
        state.exam_list_data.list = res.payload.data;
        state.exam_list_data.count = res.payload.count;
        state.corret_exam_list_loading = false;
      })
      .addCase(get_time_history.pending, (state, res: any) => {
        state.time_list_loading = true;
      })
      .addCase(get_time_history.fulfilled, (state, res: any) => {
        state.time_list_data.list = res.payload.data;
        state.time_list_data.count = res.payload.count;
        state.time_list_loading = false;
      })
      .addCase(get_corret_exam_async.fulfilled, (state, res: any) => {
        state.curr_exam = res.payload;
        state.exam_topic_list = res.payload.topic_list;

        if (state.exam_type === "exam") {
          state.current_exam_topic_id = res.payload.topic_list[0]._id;
        } else {
          const short_list = res.payload.topic_list.filter(
            (item: any) => item.topic_type === "short"
          );
          state.current_exam_topic_id = short_list[0]._id;
        }
      })
      .addCase(get_subject_one.fulfilled, (state, res: any) => {
        state.subject_one = res.payload;
      });
  },
});

export const select_cur_exam_user_list = (state: RootState) => {
  return state.subject.curr_exam_user_list;
};

export const select_cur_subject_title = (state: RootState) => {
  return state.subject.cur_subject_title;
};

export const select_cur_time_id = (state: RootState) => {
  return state.subject.cur_time_id;
};

export const select_time_area = (state: RootState) => {
  return state.subject.time_area;
};

export const select_is_show_user_edit_modal = (state: RootState) => {
  return state.subject.is_show_time_edit_modal;
};

export const select_is_show_rank_modal = (state: RootState) => {
  return state.subject.is_show_rank_modal;
};

// 获取剩余时间
export const select_rest_time = (state: RootState) => {
  return state.subject.rest_time;
};

// 获取题目类型
export const select_exam_topic_selected = (state: RootState) => {
  return state.subject.selected;
};

// 获取loading状态
export const select_subject_loading = (state: RootState) => {
  return state.subject.loading;
};

// 获取课程树形数据
export const select_subject_tree = (state: RootState) => {
  return state.subject.subject_tree;
};

// 获取当前选择课程
export const select_active_two = (state: RootState) => {
  return state.subject.active_two;
};

// 获取题目列表
export const select_topic_two_list = (state: RootState) => {
  return state.subject.topic_two_list;
};

// 获取当前选择题目
export const select_active_topic = (state: RootState) => {
  return state.subject.active_topic;
};

export const select_current_two_subject = (state: RootState) => {
  return state.subject.current_two_subject;
};

export const select_exam_topic_list = (state: RootState) => {
  return state.subject.exam_topic_list;
};

export const select_curr_exam = (state: RootState) => {
  return state.subject.curr_exam;
};

export const select_corret_exam_list_loading = (state: RootState) => {
  return state.subject.corret_exam_list_loading;
};

export const select_time_list_loading = (state: RootState) => {
  return state.subject.time_list_loading;
};

// 注意这里
export const select_current_exam_topic = (state: RootState) => {
  return (
    state.subject.exam_topic_list.find((item: any) => {
      return item._id === state.subject.current_exam_topic_id;
    }) || {}
  );
};

export const select_exam_history_data = (state: RootState) => {
  return state.subject.exam_list_data;
};

export const select_time_history_data = (state: RootState) => {
  return state.subject.time_list_data;
};

export const select_subject_one = (state: RootState) => {
  return state.subject.subject_one;
};

export const {
  set_exam_type,
  set_exam_topic_selected,
  set_subject_active_two,
  set_subject_active_topic,
  set_current_two_subject,
  set_exam_topic_list,
  set_current_exam_topic_id,
  set_exam_answer,
  set_exam_corret,
  set_exam_list_data,
  set_rest_time,
  set_time_list_data,
  set_is_show_time_edit_modal,
  set_is_show_rank_modal,
  set_time_area,
  set_cur_time_id,
  set_cur_exam_user_list,
  set_cur_subject_title,
} = subjectSlice.actions;

export default subjectSlice.reducer;
