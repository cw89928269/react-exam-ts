// @ts-nocheck
import { io } from 'socket.io-client';
import EventBus from './event'

const url = 'http://localhost:7001';

export const socket = io(url, {
    transports: ["websocket"],
});

socket.on("connect", () => {
    // state.connected = true;
    console.log('socket已经链接')
});

socket.on("disconnect", () => {
    // state.connected = false;
    console.log('socket已经断开链接')
});

socket.on("message", (e, data) => {
    // alert('收到服务到推送的消息');
    console.log('收到服务到推送的消息', e, data);
    EventBus.emit('set_socket_message', data)
});